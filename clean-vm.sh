#!/bin/bash
for vm in $(virsh list --name |grep atomic); do
        echo "Cleaning up vm $vm"
        virsh destroy $vm
        virsh undefine $vm
done
