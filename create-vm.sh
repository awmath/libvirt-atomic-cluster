#!/bin/bash
NUMNODES=${2:-2}
IMAGEFILE=$1
HOSTFULL=$(hostname -f)
HOSTSHORT=$(hostname -s)

if [ -z "$IMAGEFILE" ]; then
        echo "No image given"
        exit
fi


if [ -z $(virsh net-list --all --name | grep atomic-net) ]; then
        echo "Creating virtual network atomic-net"
        virsh net-start atomic-net
else
        if [

CURRENTDIR=$(pwd)

for type in master $(seq -f 'node%g' 1 $NUMNODES); do

if [ $(virsh list --name --all |grep atomic-$type) ]; then
        if [ $(virsh domstate atomic-$type) == "running" ]; then
                echo "atomic-$type already exists and is running"
                continue
        elif [ $(virsh domstate atomic-$type) == "shut off" ]; then
                echo "atomic-$type already exists but is destroyed ... removing"
                virsh undefine atomic-$type
        elif [ $(virsh domstate atomic-$type) == "paused" ]; then
                echo "atomic-$type already exists but is paused ... resuming"
                virsh resume atomic-$type
                continue
        else
                echo "atomic-$type is in an undefined state ... removing"
                virsh destroy atomic-$type
                virsh undefine atomic-$type
        fi
fi

mkdir -p ./vm/$type && cd ./vm/$type

rsync --progress $IMAGEFILE ./atomic-host-$type.qcow2
qemu-img create -q -f qcow2 ./atomic-storage-$type.qcow2 10G

cat << EOF > ./meta-data
instance-id: atomic-$type-$HOSTSHORT
local-hostname: atomic-$type.$HOSTFULL
EOF

cat << EOF > ./user-data
#cloud-config
password: atomic
ssh_pwauth: True
chpasswd: { expire: False }
ssh_authorized_keys:
EOF

cat $CURRENTDIR/ssh-keys.txt |sed 's/^/  - /g' >> ./user-data
genisoimage -quiet -output init.iso -volid cidata -joliet -rock user-data meta-data

if [ $type = "master" ]; then
	MEM=1024
else
	MEM=2048
fi

virt-install -q --hvm --noautoconsole --name atomic-$type --memory $MEM --os-type=linux --os-variant fedora25 --cdrom ./init.iso --disk ./atomic-host-$type.qcow2 --disk ./atomic-storage-$type.qcow2 --network bridge=virbr0 --graphics none

rm user-data meta-data

cd $CURRENTDIR

done

